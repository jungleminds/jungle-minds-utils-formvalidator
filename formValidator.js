/*
    Form Validation Utility
    by: JungleMinds.com
    created: 02-05-2016
    last modified: 12-08-2016 By Marvin de Bruin
*/

var regExs = {
    filled: '^(?!$|\\s+)',
    text: '^(?!$|\\s+)',
    select: '^(?!$|\\s+)',
    radio: '^(?!$|\\s+)',
    file: '^(?!$|\\s+)',
    email: '^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$',
    telephone: /^((\+|00(\s|\s?\-\s?)?)31(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)[1-9]((\s|\s?\-\s?)?[0-9]){8}$/
};

var options = {
    defaultValidation: false,
    watch: true,
    showClasses: false,
    classTarget: 'self',
    classPrefix: 'validator--'
};

var model = {};

var initForm = function(form, _options) {

    if (typeof form.$validator !== 'undefined') {
        return form;
    }

    if (typeof _options === 'object') {
        for (var key in _options) {
            options[key] = _options[key];
        }
    }

    form.$validator = options;

    if (!options.defaultValidation) {
        form.setAttribute('novalidate', 'novalidate');
    }

    var fields = form.querySelectorAll('input, textarea, select');
    var fieldsLength = fields.length;
    var i = fieldsLength;

    var name = form.getAttribute('name');

    if (name === null) {
        name = 'form--' + Math.round(Math.random() * 10000);
        form.setAttribute('name', name);
    }

    if (typeof model[name] === 'undefined') {
        model[name] = {};
    }

    while (i--) {
        if (typeof fields[i].$validator === 'undefined') {
            fields[i] = initField(fields[i], form);
        }
    }

    showErrors(name);

    form.addEventListener('submit', function() {
        i = fieldsLength;

        while (i--) {
            if (typeof fields[i].$validator !== 'undefined') {
                fields[i].$validator.dirty = true;
                fields[i].$validator.touched = true;
                fields[i].$validator.pristine = false;
                validateField(fields[i]);
            }
        }
    });

    return form;
};

var initField = function(field, form) {
    var fieldType = getFieldType(field);

    if (!fieldType) {
        return field;
    }

    field.$validator = {
        type: fieldType,
        pattern: field.getAttribute('data-pattern'),
        valid: false,
        required: false,
        pristine: true,
        touched: false,
        dirty: false
    };

    if (field.$validator.pattern === null) {
        field.$validator.pattern = regExs[field.$validator.type];
    }
    field.$validator.pattern = new RegExp(field.$validator.pattern);

    if (field.getAttribute('required') !== null || field.getAttribute('data-required') === 'true') {
        field.$validator.required = true;
    }

    if (options.watch) {
        field.addEventListener('keydown', changeField);
        field.addEventListener('paste', changeField);
        field.addEventListener('input', changeField);
    }
    field.addEventListener('change', changeField);
    field.addEventListener('blur', blurField);

    var name = field.getAttribute('name');

    if (name === null) {
        name = 'field--' + Math.round(Math.random() * 10000);
        field.setAttribute('name', name);
    }
    field.$validator.name = name;

    if (typeof form !== 'undefined') {
        field.$validator.form = form.getAttribute('name');
        if (field.$validator.type === 'radio') {
            model[form.getAttribute('name')][field.getAttribute('name') + '--' + field.getAttribute('value')] = field.$validator;
        } else {
            model[form.getAttribute('name')][field.getAttribute('name')] = field.$validator;
        }

    }

    return attachClasses(field);
};

var validateForm = function(form) {
    var output = model[form.getAttribute('name')];

    if (typeof model[form.getAttribute('name')] === 'undefined') {
        initForm(form);
    }

    output.valid =  true;

    for (var field in model[form.getAttribute('name')]) {
        if (model[form.getAttribute('name')][field].required && !model[form.getAttribute('name')][field].valid) {
            output.valid =  false;
        }
    }

    return output;
};

var validateField = function(field) {
    if (typeof field.$validator === 'undefined') {
        field = initField(field);
    }

    if (field.$validator.type === 'radio') {
        field.$validator.value = '';
        if (field.checked) {
            field.$validator.value = field.value;
        }
    } else {
        field.$validator.value = field.value;
    }

    field.$validator.valid = field.$validator.pattern.test(field.$validator.value);

    var hasContent = new RegExp(regExs.filled).test(field.$validator.value);

    if (hasContent) {
        field.$validator.dirty = true;
    }

    if (field.$validator.required) {
        if (!hasContent) {
            field.$validator.valid = false;
        }
    }

    if (field.$validator.type === 'radio') {
        model[field.$validator.form][field.getAttribute('name') + '--' + field.getAttribute('value')] = field.$validator;
    } else {
        model[field.$validator.form][field.getAttribute('name')] = field.$validator;
    }

    showErrors(field.$validator.form);

    return attachClasses(field);
};

var getFieldType = function(field) {
    var type = field.type;

    if (field.getAttribute('data-type') !== null) {
        type = field.getAttribute('data-type');
    }

    switch (type) {
        case 'email': return 'email';
        case 'text': return 'text';
        case 'tel': return 'telephone';
        case 'telephone': return 'telephone';
        case 'radio': return 'radio';
        case 'select-one': return 'select';
        case 'file': return 'file';
        default: return false;
    }
};

var blurField = function(e) {
    this.$validator.touched = true;
    validateField(this).removeEventListener('blur', blurField);
};

var changeField = function(e) {
    this.$validator.pristine = false;

    validateField(this);

    if (!options.watch) {
        this.removeEventListener('change', changeField);
    }
};

var attachClasses = function(field) {
    if (options.showClasses) {
        var targetField = field;
        if (options.classTarget === 'parent') {
            targetField = field.parentNode;
        }
        var classes = ['valid', 'pristine', 'touched'];
        var classList = targetField.getAttribute('class');

        if (classList === null) {
            classList = [];
        } else {
            classList = classList.split(options.classPrefix + 'valid').join('');
            classList = classList.split(options.classPrefix + 'invalid').join('');
            classList = classList.split(options.classPrefix + 'pristine').join('');
            classList = classList.split(options.classPrefix + 'touched').join('');
            classList = classList.split(options.classPrefix + 'dirty').join('');
            classList = classList.split(' ');
        }

        if (field.$validator.valid) {
            classList.push(options.classPrefix + 'valid');
        } else {
            classList.push(options.classPrefix + 'invalid');
        }

        if (field.$validator.pristine) {
            classList.push(options.classPrefix + 'pristine');
        }

        if (field.$validator.touched) {
            classList.push(options.classPrefix + 'touched');
        }

        if (field.$validator.dirty) {
            classList.push(options.classPrefix + 'dirty');
        }

        var i = classList.length;

        while (i--) {
            if (classList[i] === '') {
                classList.splice(i, 1);
            }
        }

        targetField.setAttribute('class', classList.join(' '));
    }

    return field;
};

var showErrors = function(formName) {
    var errors = document.querySelectorAll('[data-validatorError*=' + formName + ']');
    var i = errors.length;

    while (i--) {
        var errorCondition = errors[i].getAttribute('data-validatorError').split(formName).join('model.' + formName);
        var classList = errors[i].getAttribute('class');

        if (classList === null) {
            classList = [];
        } else {
            classList = classList.split(options.classPrefix + 'show').join('');
            classList = classList.split(' ');
        }

        if (eval(errorCondition)) {
            classList.push(options.classPrefix + 'show');
        }

        errors[i].setAttribute('class', classList.join(' '));
    }
};

module.exports = {
    init: initForm,
    validate: validateForm
};
