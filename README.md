# Form Validation Utility
##### by: JungleMinds.com
##### created: 02-05-2016
##### last modified: 03-05-2016 By Marvin de Bruin

usage:
```javascript
Validate = require('formValidation.js');
```

Requirements:
- The form needs a name attribute!

Initialization:
```javascript
Validator.init(<formElement>, {
    defaultValidation: false, // Prevent html5 form validation
    watch: true, // watch the field input for input / keys
    showClasses: false, // attach css classes to the form fields for all the states (see states)
    classTarget: 'self', // attach classes to self (can also be parent)
    classPrefix: 'validator--' // the class prefix, only usefull when showing classes
}); // returns the formElement
```

States (per field):
- valid: (Boolean) Tells whether an item is currently valid based on the pattern.
- invalid: (Boolean) Tells whether an item is currently invalid based on the pattern.
- pristine: (Boolean) Tells whether an input has not been used yet.
- dirty: (Boolean) Tells whether an input has been used.
- touched: (Boolean) Tells whether an input has been blurred.

Attributes (per field, optional):
- data-type (string){text|email|tel/telephone} overwrite basic field type
- data-required (boolean){true|false} makes field required or not, overwrites required attribute
- data-pattern (regEx String) overwrites the type pattern

Error reporting:
You can place errors anywhere you want, just make sure they have the data-validatorError attribute.
In this attribute you place a valid javascript "if" statement.
You can apply logic to the properties like this: formname.fieldname.property && !formname.fieldname.property
The properties available per field are:
```javascript
{
    dirty: (boolean),
    form: (string, formname),
    name: (string),
    pattern: (regEx),
    pristine: (boolean),
    required: (boolean),
    touched: (boolean),
    type: (string, text|email|tel/telephone),
    valid: (boolean),
    value: (string)
}
```

Validation:

```javascript
Validator.validate(<formElement>);
```

returns and object with the entire model and a absolute valid boolean.
```javascript
{
    field1: {
        ...
    },
    field2: {
        ...
    },
    valid: true|false
}
```

TODO:
1. more field types (numbers? checkboxes?)
2. min/max lengths
3. min/max values for numbers
4. write Readme for select & radios

## Release notes

### 1.0.1
Added basic support for upload, select and radio's

### 1.0.0
Initial release
